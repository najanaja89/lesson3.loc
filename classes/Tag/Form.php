<?php
/**
 * Class Form
 * @method self method($value);
 */

namespace Tag;
use Tag;

class Form extends NamedTag
{
    protected $name = "form";

    protected static function name(): string
    {
        return "form";
    }

    public static function input($name, $type = "text", $value = null)
    {
        $attributes = [
            "name" => $name,
            "type" => $type
        ];
        if ($value)
            $attributes["value"] = $value;

        return Form::input($attributes);
    }

    public static function label($body, $for = null)
    {
        $label = Tag::label()->appendBody($body);

        if ($for)
            $label->setAttribute("for", $for);

        return $label;
    }

}