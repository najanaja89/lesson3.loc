<?php
include_once "autoload.php";

$a = new BaseTag("a");

echo $a;


class GrandParent
{
    static $name = "Bob";
}

abstract class MyParent extends GrandParent
{
    static $name = "Lisa";

    static function getNameStatic()
    {
        return static::$name; //раннее статическое связывание
        //return self::getNameStatic(); имя будет Lisa
    }

    public function foo()
    {
        return "bar";
    }

    public function getLastName()
    {
        return " Wick";
    }

    abstract public function getName();
}

class Child extends MyParent
{
    static $name = "Hanna";

    static function getNameStatic()
    {
        return parent::$name; //echo Lisa parent only for static
    }

    public function getName()
    {
        return "John" . $this->getLastName();
    }

}


function foo2(MyParent $parent)
{
    return $parent->getName();
}


$child = new Child();

echo foo2($child) . "<br>";

echo $child->foo() . "<br>";
echo $child->getName();

$form = Form::make();

Form::input("username")->appendTo($form);

Form::input("password", "password")->appendTo($form);

$form->input("username"); //вызывается как нестатический метод через ->

echo $form;
