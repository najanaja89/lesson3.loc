<?php
spl_autoload_register(function ($name) {
    //var_dump($name); die;

    //PSR-4 стандарт
    $path = str_replace("\\", DIRECTORY_SEPARATOR, $name);
    $file = "classes" . DIRECTORY_SEPARATOR . "{$path}.php";
    if (!file_exists($file) || !is_file($file))
        die("File $file not found");

    include_once $file;

    if (!class_exists($name))
        die("Class $name not found");

});
