<?php

include_once "db_helpers.php";
include_once "classes/BaseTag.php";


////db_insert("lessons.books", [
////    "name"=>"Enimion",
////]);
//
//db_update("lessons.books", ["name" => "Enimion"], ["name" => "Endimion"]);
//$rows = db_select("lessons.books");
//print_r($rows);
//echo "<br>";
//
//class MyClass
//{
//    public $name = "John";
//
//    public function __construct($name = "")
//    {
//        $this->setName($name);
//    }
//
//    public function foo()
//    {
//        return "bar";
//        //по умолчанию public
//    }
//
//    public function getName()
//    {
//        return $this->name;
//    }
//
//    public function setName($name)
//    {
//        return $this->name = $name;
//    }
//}
//
//$myClass = new MyClass("John");
//echo $myClass->foo();
//echo "<br>";
//echo $myClass->getName();
//echo "<br>";
//$myClass->setName("Sarah");
//echo $myClass->getName();
//
//class Number
//{
//    private $num;
//
//    public function __construct($num)
//    {
//        $this->setNumber($num);
//    }
//
//    public function setNumber($num)
//    {
//        return $this->num = $num;
//
//    }
//
//    public function add($number)
//    {
//
//        return $this->setNumber($this->num+$number);
//    }
//
//    public function sub($number)
//    {
//        return $this->setNumber($this->num-$number);
//    }
//
//    public function mult($number)
//    {
//        return $this->setNumber($this->num*$number);
//    }
//
//      public function div($number)
//    {
//        return $this->setNumber($this->num/$number);
//    }
//
//    public function mod($number)
//    {
//        return $this->setNumber($this->num%$number);
//    }
//
//
//    public function getNumber()
//    {
//        return $this->num;
//    }
//
//
//}
//
//$number = new Number(2);
//echo "<br>";
//echo $number->getNumber();
//echo "<br>";
//echo $number->add(2);
//echo "<br>";
//echo $number->mult(2);
//echo "<br>";
//echo $number->div(2);
//echo "<br>";
//echo $number->sub(3);
//echo "<br>";
//echo $number->mod(3);
//echo "<br>";
//echo $number->getNumber();
//echo "<br>";


//$tag = new Tag("div");
//$tag->addClass("first");
//$tag->addClass("second");
//$tag->removeClass("second");
//
//print_r($tag->classesAsArray());
//$tag->__construct("div");
//echo $tag;


class Str
{
    static $name = "STRING";

    public static function upper($str)
    {
        return mb_strtoupper($str);
    }

    public static function slug($str)
    {
        $str = str_replace(" ", "-", $str);
        return self::upper($str);
    }

    public $age = 29;
}

//echo Str::$name;
//echo Str::upper("hello");


echo Str::slug("hello world !");

class MyCounter
{
    public static $counter = 0;

    public function __construct()
    {
        self::incrementCounter();
    }

    protected static function incrementCounter()
    {
        $file = "counter";
        if (!file_exists($file) || !is_file($file))
            self::$counter = 0;
        else
            self::$counter = file_get_contents($file);

        self::$counter++;
        file_put_contents($file, self::$counter);
    }

}

$myClass1 = new MyCounter();
$myClass2 = new MyCounter();
$myClass1 = new MyCounter();
$myClass2 = new MyCounter();


echo MyCounter::$counter;
echo "<br>";

class MyConst
{
    const name = "John";
}

echo "<br>";
echo MyConst::name;

echo "<br>";

//$link ->id();
//$link::idStatic();

//__call($name, $attributes) -> вызывается, когда вызывается несуществующий метод
//__callStatic($name, $attributes)->вызывается при отсутствий статическог метода

//
//class Test {
//    public function __call($name, $arguments)
//    {
//        var_dump($name); echo "\n";
//        var_dump($arguments);
//    }
//}
//
//$test = new Test();
//$test->hello("world", ["school"]);

//$link = Tag::make("a");
//$link->id("main");
//
//echo Tag::form()->start();
//echo Tag::input();
//echo Tag::form()->end();
//
//echo $link;

echo BaseTag::table()->appendBody("hello")->prependBody(["asd", "123"]);

$html = BaseTag::html(["lang"=>"ru"]);
$head = BaseTag::head()->appendTo($html);
$body = BaseTag::body()->appendTo($html);

$ul = BaseTag::ul()->appendTo($body);
$items = ["first", "second", "third"];

foreach ($items as $item){
    BaseTag::li()->appendBody($item)->appendTo($ul);
}

echo $html;






